package de.myedulife.esco.resource;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.myedulife.esco.model.LanguageEscoSkill;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * @author haridhra
 */
@Path("/esco")
public class EscoResource {

    public static final String EU_ESCO_API_RESOURCE_SKILL_URI = "https://ec.europa.eu/esco/api/resource/skill?uri=";

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello ESCO";
    }

    /**
     * To get the parameter title and description in a specific language.
     * @param url
     * @param lang
     * @return serialize the object LanguageEscoSkill to json
     */
    @GET
    @Path("/metadata")
    @Produces(MediaType.TEXT_PLAIN)
    public String metadata(@QueryParam("url") String url, @QueryParam("lang") String lang) {
        try {
            // lang default to "en"
            lang = (lang != null && !lang.isEmpty()) ? lang : "de";

            // Construct URL with language parameter
            String urlWithLang = EU_ESCO_API_RESOURCE_SKILL_URI + URLEncoder.encode(url, StandardCharsets.UTF_8);

            // Make GET request
            HttpURLConnection connection = (HttpURLConnection) new URL(urlWithLang).openConnection();
            connection.setRequestMethod("GET");

            // Read response
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(connection.getInputStream());

            // Extract the title
            String title = jsonNode.path("preferredLabel").path(lang).asText();

            // Extract the description for the specified language
            JsonNode descriptionNode = jsonNode.path("description").path(lang).path("literal");
            String description = descriptionNode.isMissingNode() ? "" : descriptionNode.asText();

            LanguageEscoSkill languageEscoSkill = new LanguageEscoSkill(title, description);

            // Return the extracted title and description in the json format
            return objectMapper.writeValueAsString(languageEscoSkill);

        } catch (Exception e) {
            e.printStackTrace();
            return "Unexpected error retrieving details from the specified URI: " + e.getMessage();
        }
    }
}









