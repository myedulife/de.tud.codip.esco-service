package de.myedulife.esco.model;


import lombok.Getter;

/**
 * @author bja
 */
@Getter
public class LanguageEscoSkill {

    private String title;
    private String description;

    public LanguageEscoSkill() {}

    /**
     * constructor of this class
     * @param title the title
     * @param description the description
     */
    public LanguageEscoSkill(String title, String description) {
        this.title = title;
        this.description = description;
    }
}
